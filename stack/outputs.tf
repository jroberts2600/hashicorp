output "elb_public_name" {
  value = "${aws_elb.web_cluster.dns_name}"
}