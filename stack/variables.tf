variable environment {
  type = "string"
  description = "Environment"
}

variable region {
  type = "string"
  description = "AWS region"
}

variable public_key {
  type = "string"
  description = "Public Key"
}

variable azs {
  type = "list"
  description = "AWS AZs"
}

variable ami {
  type = "string"
  description = "AMI ID"
}

variable instance_type {
  type = "string"
  description = "Instance Type"
}

variable tags {
  description = "Required tags for the environment"
  type        = "map"
  default     = {
    environment       = "dev"
    application_name  = "HashiCorp Demo"
    cost_center         = "1234"
    contact             = "John Doe"
  }
}

variable private_subnet_ids {
  type = "list"
  description = "Private Subnets"
}

variable public_subnet_ids {
  type = "list"
  description = "Public Subnets"
}

variable vpcid {
  type = "string"
  description = "VPC ID"
}