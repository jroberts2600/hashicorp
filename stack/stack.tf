provider "aws" {
	region					= "${var.region}"
	shared_credentials_file = "/Users/flu7953/.aws/credentials"
  	profile                 = "hashicorp-demo"
}

resource "aws_key_pair" "key_pair" {
	key_name   = "${var.environment}_key"
	public_key = "${var.public_key}"
}

data "template_file" "init" {
  template = "${file("${path.module}/init.tpl")}"
}

resource "aws_instance" "web" {
	count 		= 4
	ami           = "${var.ami}"
	instance_type = "${var.instance_type}"
	key_name 		= "${aws_key_pair.key_pair.key_name}"
	subnet_id		= "${var.private_subnet_ids[count.index]}"
  security_groups = ["${aws_security_group.elb_to_ec2.id}"]
  user_data = "${data.template_file.init.rendered}" ##. Why is this not working...
    tags = "${merge(
    	var.tags,
    	map("Name", "${var.environment}-webserver")
	)}"
}

resource "aws_security_group" "elb_to_ec2" {
  name = "ELB access to EC2"
  description = "LBs access to EC2"
  vpc_id = "${var.vpcid}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    self = true
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = "${merge(
      var.tags,
      map("Name", "${var.environment}-sg")
    )}"
}

resource "aws_security_group" "allow_web_traffic" {
  name = "allow_web_traffic"
  description = "Allow HTTP inbound web traffic"
  vpc_id = "${var.vpcid}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = "${merge(
      var.tags,
      map("Name", "${var.environment}-sg")
    )}"
}

resource "aws_elb" "web_cluster" {
  name = "${var.environment}-web-cluster"
	subnets = ["${var.public_subnet_ids}"]
  security_groups = ["${aws_security_group.allow_web_traffic.*.id}","${aws_security_group.elb_to_ec2.id}"]
  listener {
		instance_port		= 80
		instance_protocol	= "http"
		lb_port				= 80
		lb_protocol			= "http"
	}
	health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 30
    }
    instances = ["${aws_instance.web.*.id}"]
    tags = "${merge(
    	var.tags,
    	map("Name", "${var.environment}-webelb")
    )}"
}

# resource "aws_security_group" "test_sg" {
#  name = "Testing SGs"
#  description = "Testing SGs"
#  vpc_id = "${var.vpcid}"

#  ingress {
#    from_port = 443
#    to_port = 443
#    protocol = "tcp"
#    self = true
#  }

#  egress {
#    from_port       = 0
#    to_port         = 0
#    protocol        = "-1"
#    cidr_blocks     = ["0.0.0.0/0"]
#  }

#  tags = "${merge(
#      var.tags,
#      map("Name", "${var.environment}-sg")
#    )}"
# }