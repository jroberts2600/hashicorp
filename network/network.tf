provider "aws" {
	region					= "${var.region}"
	shared_credentials_file = "/Users/flu7953/.aws/credentials"
  	profile                 = "hashicorp-demo"
}

resource "aws_vpc" "main" {
	cidr_block = "${var.cidr_block}"
	tags = "${merge(
    var.tags,
    map(
      "Name", "${var.environment}-VPC",
      "Environment", "${var.environment}",
      )
	)}"
}

resource "aws_subnet" "public" {
	count			= "${length(var.azs)}"
	vpc_id			= "${aws_vpc.main.id}"
	cidr_block		= "${element(concat(var.public_ranges, list("")), count.index)}"
	availability_zone       = "${element(var.azs, count.index)}"
	map_public_ip_on_launch = true

	tags = "${merge(
    var.tags,
    map(
      "Name", "${var.environment}-public${count.index + 1}",
      "Environment", "${var.environment}",
      )
	)}"
}

resource "aws_subnet" "private" {
	count			= "${length(var.azs)}"
	vpc_id			= "${aws_vpc.main.id}"
	cidr_block		= "${element(concat(var.private_ranges, list("")), count.index)}"
	availability_zone       = "${element(var.azs, count.index)}"

	tags = "${merge(
    var.tags,
    map(
      "Name", "${var.environment}-private${count.index + 1}",
      "Environment", "${var.environment}",
      )
	)}"
}

resource "aws_internet_gateway" "igw" {
	vpc_id = "${aws_vpc.main.id}"
	tags = "${merge(
    	var.tags,
    	map(
      		"Name", "${var.environment}-internet-gw"
      	)
  	)}"
}

resource "aws_route_table" "private" {
	count  = "${length(var.azs)}"
	vpc_id = "${aws_vpc.main.id}"

	tags = "${merge(
    	var.tags,
    	map("Name", "${var.environment}-private-az${count.index + 1}")
	)}"
}

resource "aws_route_table" "public" {
	count  = "${length(var.azs)}"
	vpc_id = "${aws_vpc.main.id}"

	tags = "${merge(
		var.tags,
		map("Name", "${var.environment}-public-az${count.index + 1}")
	)}"
}

resource "aws_route_table_association" "private" {
	count          = "${aws_subnet.private.count}"
	subnet_id      = "${aws_subnet.private.*.id[count.index]}"
	route_table_id = "${aws_route_table.private.*.id[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index]}"
}

resource "aws_route" "internet_access" {
	route_table_id         = "${aws_vpc.main.main_route_table_id}"
	destination_cidr_block = "0.0.0.0/0"
	gateway_id             = "${aws_internet_gateway.igw.id}"
}

resource "aws_route" "nat_access" {
	count				   = "${aws_subnet.private.count}"
	route_table_id         = "${aws_route_table.private.*.id[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index]}"
	destination_cidr_block = "0.0.0.0/0"
	gateway_id             = "${aws_nat_gateway.nat.*.id[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index]}"
}

resource "aws_nat_gateway" "nat" {
	count         = "${length(var.azs)}"
	allocation_id = "${aws_eip.nat_eip.*.id[count.index]}"
	subnet_id     = "${aws_subnet.public.*.id[count.index]}"

	tags = "${merge(
		var.tags,
		map("Name", "${var.environment}-public${count.index + 1}")
	)}"
}

resource "aws_eip" "nat_eip" {
	count = "${length(var.azs)}"
	vpc   = true

	tags = "${merge(
		var.tags
	)}"
}
