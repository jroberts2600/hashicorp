variable name {
  type = "string"
  description = "VPC Name"
}

variable environment {
  type = "string"
  description = "Environment"
}

variable region {
  type = "string"
  description = "AWS region"
}

variable azs {
  type = "list"
  description = "AWS AZs"
}

variable cidr_block {
  type = "string"
  description = "IP CIDR Block"
}

variable private_ranges {
  type = "list"
  description = "Private Subnets"
}

variable public_ranges {
  type = "list"
  description = "Public Subnets"
}

variable private_sn {
  type = "string"
  description = "Private and public subnets"
  default = "false"
}

variable "tags" {
  description = "Required tags for the environment"
  type        = "map"
  default     = {
    environment       = "dev"
    application_name  = "HashiCorp Demo"
    cost_center         = "1234"
    contact             = "John Doe"
  }
}